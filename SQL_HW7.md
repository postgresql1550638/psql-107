// q1
SELECT rating, 	COUNT(*) from film
GROUP BY rating;

// q2
SELECT replacement_cost, COUNT(*) from film
GROUP BY replacement_cost
HAVING  COUNT(*) > 50;

// q3
SELECT store_id, COUNT(*) from customer
GROUP BY store_id;

// q4
SELECT country_id, COUNT(*) from city
GROUP BY country_id
ORDER BY COUNT(*) DESC
LIMIT 1;